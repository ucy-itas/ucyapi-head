<?php
namespace Iss\Api\Service\Head;

use Iss\Api\Service\Core\Representation\Formatter\Blank;
use Iss\Api\Service\Core\Representation\Representation;
use Iss\Api\ServiceInterface;
use Iss\Api\ServiceTrait;
use Phalcon\Mvc\Micro;
use Phalcon\Http\{Response, Request};
use Phalcon\Events\Event;

class Head implements ServiceInterface
{
    use ServiceTrait;

    protected bool $enabled = false;

    public function register(Micro $application): bool
    {
        if (!$application->getDI()->has('representation')) {
            // authorization service is not ready yet
            return false;
        }

        /**
         * @var $representation Representation
         */
        $representation = $application['representation'];
        $this->setPriorityAfter($representation);

        $application->eventsManager->attach('micro:afterExecuteRoute', $this, $this->getPriority());
        $application->eventsManager->attach('micro:beforeExecuteRoute', $this, $this->getPriority());
        $application->eventsManager->attach('micro:beforeHandleRoute', $this, $this->getPriority());
        $application->setService(self::getName(), $this, true);
        return true;
    }

    public function unregister(Micro $application): ?ServiceInterface
    {
        $application->getEventsManager()->detach('micro:afterExecuteRoute', $this);
        $application->getEventsManager()->detach('micro:beforeExecuteRoute', $this);
        $application->getEventsManager()->detach('micro:beforeHandleRoute', $this);
        $application->getDI()->remove(self::getName());
        return $this;
    }

    public static function getName(): string
    {
        return 'head';
    }

    public function beforeHandleRoute(Event $event, Micro $application)
    {
        $request = $application['request'];
        if ($request->isHead()) {
            $logger = $application['logger'];
            $logger['service'] = self::getName();
            $logger->notice('[%service%] Changing method to GET [%uuid%]');

            $_SERVER['REQUEST_METHOD'] = 'GET';
            $this->enabled = true;
            /**
             * @var $representation Representation
             */
            $representation = $application['representation'];
            $formatter = $representation->getFormatter();
            if (!$formatter instanceof Blank) {
                $representation->setFormatter(new Blank($formatter));
            }
        }
    }

    public function beforeExecuteRoute(Event $event, Micro $application)
    {
        if ($this->enabled) {
            /**
             * @var $representation Representation
             */
            $representation = $application['representation'];
            $formatter = $representation->getFormatter();
            if (!$formatter instanceof Blank) {
                $representation->setFormatter(new Blank($formatter));
            }
        }
    }

    public function afterExecuteRoute(Event $event, Micro $application)
    {
        if ($this->enabled) {
            $logger = $application['logger'];
            $logger['service'] = self::getName();
            $logger->notice('[%service%] Changing method to HEAD [%uuid%]');
            $_SERVER['REQUEST_METHOD'] = 'HEAD';
        }
    }
}